const path = require("path");

let config = {

    mode: "none",
    devtool: "source-map",
    entry: "./src/leaflet.canvasmarker.js",
    output: {
        filename: 'Leaflet.canvasmarker.js',
        path: path.resolve(__dirname, 'dist'),
        libraryTarget: 'umd'
    }
};

module.exports = config;
