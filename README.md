# Leaflet-CanvasMarker

 在Canvas上绘制Marker,而不是每个marker插件一个dom节点,极大地提高了渲染效率。主要代码参考自 https://github.com/eJuke/Leaflet.Canvas-Markers  ，不过此插件有些Bug，github国内不方便，作者也不维护了，所以在gitee上新建一个仓库进行维护与升级。 [demo](http://examples.panzhiyue.website/Leaflet-CanvasMarker/examples/index.html)



## npm下载

```bash
npm i @panzhiyue/leaflet-canvasmarker
```

## 使用

```javascript
import {CanvasMarkerLayer} from "@panzhiyue/leaflet-canvasmarker"
```



## 示例 

```javascript
    // 创建图层
    var ciLayer = L.canvasMarkerLayer({
      collisionFlg: true
    }).addTo(map);

    var icon = L.icon({
      iconUrl: 'img/pothole.png',
      iconSize: [20, 18],
      iconAnchor: [10, 9]
    });
    // 定义Marker
    var markers = [];
    for (var i = 0; i < 100000; i++) {
      var marker = L.marker([58.5578 + Math.random() * 1.8, 29.0087 + Math.random() * 3.6], {
        icon: icon,
        zIndex: 2
      }).bindPopup("I Am " + i);
      markers.push(marker);
    }
    // 把marker添加到图层
    ciLayer.addLayers(markers);
    //定义事件
    ciLayer.addOnClickListener(function (e, data) {
      console.log(data)
    });
    ciLayer.addOnHoverListener(function (e, data) {
      console.log(data[0].data._leaflet_id)
    });
```

## 效果图

### 1.启用碰撞检测

![image-20211020100758268](https://pzy-images.oss-cn-hangzhou.aliyuncs.com/img/202111181937529.webp)

### 2.禁用碰撞检测

![image-20211020100837628](https://pzy-images.oss-cn-hangzhou.aliyuncs.com/img/202111181937531.webp)



## 参数

### collisionFlg

- **类型：boolean**
- **默认值：false**

配置是否启用碰撞检测，即重叠图标只显示一个



### moveReset

- **类型：boolean**
- **默认值：false**

在move时是否刷新地图



### zIndex

- **类型：number**
- **默认值：null**

Leaflet.Marker对象zIndex的默认值

### opacity

- **类型：number**
- **默认值：1**

图层的不透明度,0(完全透明)-1(完全不透明)

## Leaflet.Marker扩展参数

### zIndex

- **类型:number**

显示顺序



## Leaflet.Icon扩展参数

### rotate

- **类型:number**

旋转角度:Math.PI/2

## 方法

- **addLayer(marker)**：向图层添加标记。
- **addLayers(markers)**：向图层添加标记。
- **removeLayer(marker, redraw)**：从图层中删除一个标记。`redraw`为true时删除后直接重绘,默认为true
- **redraw()** : 重绘图层
- **addOnClickListener(eventHandler)**：为所有标记添加通用点击侦听器
- **addOnHoverListener(eventHandler)**：为所有标记添加悬停监听器
- **addOnMouseDownListener(eventHandler)**：为所有标记添加鼠标按下监听器
- **addOnMouseUpListener(eventHandler)**：为所有标记添加鼠标松开监听器
- **setOpacity(opacity)**:设置图层不透明度

