# [2.1.5](https://gitee.com/panzhiyue/Leaflet-CanvasMarker/compare/v2.1.4...v2.1.5) (2022-05-30)

### Bug修复

- **types:** [修复无_leaflet_id的问题](https://gitee.com/panzhiyue/Leaflet-CanvasMarker/commit/8beeb452adb9f133fb00b1033229259f96fabd8e)  ([8beeb45](https://gitee.com/panzhiyue/Leaflet-CanvasMarker/commit/8beeb452adb9f133fb00b1033229259f96fabd8e)) [@k1nZ](https://gitee.com/k1nZer0)

- **types:** 修复reset不重绘的问题

- **types:** 修复无法点击的bug

  

## 优化

- **types:** 优化不需要检查碰撞逻辑时的加载速度